import SwiftUI

@main
struct RouterSpikeApp: App {
    var body: some Scene {
        WindowGroup {
            Router(
                routes: ["login/email": { AnyView(EmailView()) },
                         "login/password": { AnyView(PasswordView()) },
                         "conversation/id": { AnyView(MessagesView()) },
                         "conversations": { AnyView(ChatListView()) }]) {
                AppCoordinator()
            }
        }
    }
}
