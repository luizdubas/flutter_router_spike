import SwiftUI

struct Router<Content: View>: View {
    private let content: () -> Content
    private let routing: Routing
    
    @inlinable public init(
        routes: [String: () -> AnyView],
        @ViewBuilder content: @escaping () -> Content
    ) {
        self.routing = Routing(routes: routes)
        self.content = content
    }
    
    var body: some View {
        NavigationView {
            content().environmentObject(routing)
        }
    }
}

class Routing: ObservableObject {
    let routes: [String: () -> AnyView]
    
    init(routes: [String: () -> AnyView]) {
        self.routes = routes
    }
    
    func navigate(to route: String) -> some View {
        return LazyView(self.getView(from: route).environmentObject(self))
    }
    
    private func getView(from route: String) -> some View {
        return routes[route]?() ?? AnyView(ErrorView())
    }
}

struct ErrorView: View {
    var body: some View {
        Text("An error happened while navigating")
    }
}
	
