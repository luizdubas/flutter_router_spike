import SwiftUI

struct AppCoordinator: View {
    @EnvironmentObject var routing: Routing
    @ObservedObject var appStateManager: AppStateManager = .sharedInstance
    
    var body: some View {
        view(from: appStateManager.appState)
    }
    
    func view(from state: AppState) -> some View {
        switch state {
        case .unauthenticated:
            return routing.navigate(to: "login/email")
        case .authenticated:
            return routing.navigate(to: "conversations")
        }
    }
    
}

struct AppCoordinator_Previews: PreviewProvider {
    static var previews: some View {
        AppCoordinator()
    }
}
