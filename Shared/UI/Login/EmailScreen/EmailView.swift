import SwiftUI

struct EmailView: View {
    @EnvironmentObject var routing: Routing
    @State var email: String = ""
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text("Type your email")
                    .font(.title)
                    .padding()
                Spacer()
                TextField("email", text: $email)
                    .textContentType(.emailAddress)
                    .padding()
                Spacer()
                NavigationLink("Next",
                               destination: routing.navigate(to: "login/password"))
                    .padding()
            }
            Spacer()
        }
    }
    
    enum Actions {
        case next
    }
}

struct EmailView_Previews: PreviewProvider {
    static var previews: some View {
        EmailView ()
    }
}

