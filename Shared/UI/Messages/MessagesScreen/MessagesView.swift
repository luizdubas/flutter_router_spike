import SwiftUI

struct MessagesView: View {
    var body: some View {
        VStack {
            Spacer()
            Text("Messages would appear here!")
                .padding()
            Spacer()
        }
    }
    
    enum Actions {
        case close
    }
}

struct MessagesView_Previews: PreviewProvider {
    static var previews: some View {
        MessagesView()
    }
}
