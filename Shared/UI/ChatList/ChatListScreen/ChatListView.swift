import SwiftUI

struct ChatListView: View {
    @ObservedObject var appStateManager: AppStateManager = .sharedInstance
    let chatList: [ChatModel] = defaultChatList
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("Chat list")
                .padding()
            Spacer()
            List(chatList) { (model) -> ChatRow in
                ChatRow(model: model)
            }
            Spacer()
            Button("Logout") {
                appStateManager.logout()
            }.padding()
        }
    }
    
    enum Actions {
        case open(id: String)
        case logout
    }
}

let defaultChatList: [ChatModel] = [
    .init(id: "1"),
    .init(id: "2"),
    .init(id: "3"),
    .init(id: "4"),
    .init(id: "5"),
    .init(id: "6")
]

struct ChatModel: Identifiable {
    let id: String
}

struct ChatListView_Previews: PreviewProvider {
    static var previews: some View {
        ChatListView()
    }
}
