import SwiftUI

struct ChatRow: View {
    @EnvironmentObject var routing: Routing
    @State var openSheet: Bool = false
    let model: ChatModel
    
    var body: some View {
        VStack {
            HStack {
                Text("Person name").font(.headline)
                Spacer()
                Text("Date").font(.caption)
            }
            HStack {
                Text("Last message").font(.body)
                Spacer()
            }.padding(EdgeInsets(top: 16, leading: 0, bottom: 0, trailing: 0))
        }
            .padding(EdgeInsets(top: 8, leading: 0, bottom: 8, trailing: 0))
            .onTapGesture { openSheet = true }
            .sheet(isPresented: $openSheet) {
                routing.navigate(to: "conversation/id")
        }
    }
}

struct ChatRow_Previews: PreviewProvider {
    static var previews: some View {
        ChatRow(model: .init(id: "1"))
    }
}
