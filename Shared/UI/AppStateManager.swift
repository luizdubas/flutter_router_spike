import Foundation
import Combine

class AppStateManager: ObservableObject {
    static let sharedInstance = AppStateManager()
    @Published var appState: AppState = .unauthenticated
    
    private init() {}
    
    func login() {
        appState = .authenticated
    }
    
    func logout() {
        appState = .unauthenticated
    }
}

enum AppState {
    case unauthenticated
    case authenticated
}
