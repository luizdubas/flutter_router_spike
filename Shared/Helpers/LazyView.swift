import SwiftUI

///Delays the creation of the view to the moment that it will be shown
///https://www.objc.io/blog/2019/07/02/lazy-loading/
struct LazyView<Content: View>: View {
    let build: () -> Content
    
    init(_ build: @autoclosure @escaping () -> Content) {
        self.build = build
    }
    
    var body: Content {
        build()
    }
}
